# ShoppingCart

This is simple shopping cart implementation in Java SE. In cart we have list of choosen products with quantity of every position. 
We can add to cart products stored in shop (if we add the same product many times, there still is only one cart position - only quantity grows), 
delete choosen products	and also find product in cart by name. Cart also have methods to return value of all products that are in, method to 
return total quantity of all products in cart, and method returning cart content as a list of cartPositions. Application has simple console user 
interface to demonstrate how app works. 

In future is planning to join model classses with database and GUI.

## Tests
	
Application has written unit tests for model classes (Product, CartPosition, Cart)
	
	
## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Author

* **Marcin Makiewicz** [My Bitbucket Repository](https://bitbucket.org/Marcin_Makiewicz/)