package makiewicz.task.model;

import java.math.BigDecimal;

public class CartPosition {

    private Product product;
    private int quantity;

    public CartPosition() {
        this.quantity = 0;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount(){
        BigDecimal productPrice = BigDecimal.valueOf(product.getPrice());
        BigDecimal productQuantity = new BigDecimal(quantity);
        return productPrice.multiply(productQuantity);
    }
}
