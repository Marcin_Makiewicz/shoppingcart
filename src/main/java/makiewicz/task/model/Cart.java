package makiewicz.task.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Cart {

    private final List<CartPosition> cartContent = new ArrayList<>();

    public Cart() {
    }

    public List<CartPosition> getCartContent() {
        return cartContent;
    }

    private CartPosition findPositionInCart (Product product){
        for (CartPosition position: cartContent) {
            if (position.getProduct().equals(product)){
                return position;
            }
        }
        return null;
    }

    public void addProduct(Product product, int quantity){
        CartPosition cartPosition = findPositionInCart(product);

        if(cartPosition==null){
            cartPosition = new CartPosition();
            cartPosition.setQuantity(0);
            cartPosition.setProduct(product);
            cartContent.add(cartPosition);
        }

        int newQuantity = cartPosition.getQuantity() + quantity;
        if(newQuantity<=0){
            cartContent.remove(cartPosition);
        } else {
            cartPosition.setQuantity(newQuantity);
        }
    }

    public double getTotalValue(){
        BigDecimal totalValue = BigDecimal.ZERO;
        for (CartPosition position: cartContent) {
            totalValue = totalValue.add(position.getAmount());
        }
        return totalValue.doubleValue();
    }

    public int getTotalQuantity(){
        int quantity = 0;
        for (CartPosition position: cartContent) {
            quantity+=position.getQuantity();
        }
        return quantity;
    }

    public Optional<Product> findProductByName(String name){
        Product product = null;
        for (CartPosition position: cartContent) {
            if(position.getProduct().getName().equals(name)){
                product = position.getProduct();
            }
        }
        return Optional.ofNullable(product);
    }
}
