package makiewicz.task.userInterface;

import makiewicz.task.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsLoader {

    public static List<Product> storeProducts() {
        List<Product> products = new ArrayList<>();

        String [] productNames = {"Toyota Corolla", "Ford Mondeo", "Opel Astra"};
        String [] productDescriptions = {"Fast japan Car", "Big car for family", "Medium-size car for everyone"};
        Double [] productPrices = {50000.00,69999.99,39000.00};

        for (int i = 0; i < productNames.length; i++) {
            products.add(new Product(productNames[i],productDescriptions[i],productPrices[i]));
        }

        return products;
    }



}
