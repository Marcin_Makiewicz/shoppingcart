package makiewicz.task.userInterface;

import makiewicz.task.model.Cart;
import makiewicz.task.model.CartPosition;
import makiewicz.task.model.Product;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class UI {

    Cart cart = new Cart();
    List<Product> storedProducts = ProductsLoader.storeProducts();
    private int userChoice;

    public UI() {
        menu();
    }

    public void startScreenMenu () {
        System.out.println("Hello, this is simple shopping cart:");
        System.out.println("1. Display products in shop");
        System.out.println("2. Display Your cart content");
        System.out.println("0. Exit program");
    }

    public void shopProductsMenu() {
        System.out.println("1. Add something to Cart");
        System.out.println("2. Remove something From Cart");
        System.out.println("3. Exit");
    }

    public void menu () {
        do {
            startScreenMenu();
            getUserInput();

            switch (userChoice) {
                case 1:
                    displayAllStoredProducts();
                    shopProductsMenu();
                    getUserInput();
                    userChoice1();
                    break;
                case 2:
                    showCart();
                    shopProductsMenu();
                    getUserInput();
                    userChoice1();
                    break;
                case 0:
                    System.out.println("Program is closed");
                    System.exit(0);
                    break;
                default:
                    break;
            }
        } while (userChoice != 0);
    }

    private void userChoice1() {
        switch (userChoice) {
            case 1:
                addProductToCart();
                showCart();
                break;
            case 2:
                removeProductFromCart();
                showCart();
                break;
            default:
                break;
        }
    }

    public void getUserInput(){
        Scanner scanner = new Scanner(System.in);
        try {
            userChoice = scanner.nextInt();
        }catch (InputMismatchException e){
            System.out.println("ATENTION: wrong type of data - write a number:");
            getUserInput();
        }
    }

    private void displayAllStoredProducts() {
        for (Product product : storedProducts) {
            System.out.println("Product: " + product.getName());
            System.out.println("Description: " + product.getDescription());
            System.out.println("Price: " + product.getPrice()+"\n");
        }
    }

    private void showCart(){
        System.out.println("\nCART CONTENT: ");
        for (CartPosition cartPosition:cart.getCartContent()) {
            System.out.println("Product: " + cartPosition.getProduct().getName() +
                    "  Price: " + cartPosition.getProduct().getPrice()+
                    "  Quantity: "+cartPosition.getQuantity());
        }
        System.out.println("Number of products in cart: " + cart.getTotalQuantity());
        System.out.println("Value of all products in cart: " + cart.getTotalValue()+"\n");
    }

    private void addProductToCart() {
        System.out.println("Write a name of product, that you want to add to cart: ");
        String name = new Scanner(System.in).nextLine();
        Optional<Product> optional = findProductByName(name);
        if(optional.isPresent()){
            cart.addProduct(optional.get(),1);
        }else{
            System.out.println("ATTENTION!, we don't have such product!\n");
        }
    }

    private void removeProductFromCart() {
        System.out.println("Write a name of product, that you want to add to cart: ");
        String name = new Scanner(System.in).nextLine();
        Optional<Product> optional = findProductByName(name);
        if(optional.isPresent()){
            cart.addProduct(optional.get(),-1);
        }else{
            System.out.println("ATTENTION!, we don't have such product!\n");
        }
    }

    public Optional<Product> findProductByName(String name){
        Product product = null;
        for (Product position: storedProducts) {
            if(position.getName().equals(name)){
                product = position;
            }
        }
        return Optional.ofNullable(product);
    }
}
