package makiewicz.task.model;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CartTest {

    private Cart cart;
    private Product product;

    @Before
    public void setup(){
        cart = new Cart();
        product = new Product("Book","Good book for kids",19.99);
    }

    @Test
    public void emptyCartSchouldReturnEptyList(){
        Assert.assertTrue(cart.getCartContent().isEmpty());
    }

    @Test
    public void cartSchouldHaveOnePositionAfterAddingTheSameProductaCoupleOfTimes(){
        cart.addProduct(product,1);
        cart.addProduct(product,2);
        cart.addProduct(product,1);
        Assert.assertEquals(cart.getCartContent().size(),1);
    }

    @Test
    public void cartSchouldReturnTotalValueOfProductsStoredInCart(){
        cart.addProduct(product,4);
        cart.addProduct(new Product("Cola","Coca cola 0,5l",2.99),3);
        cart.addProduct(product,2);
        Assert.assertEquals(cart.getTotalValue(),128.91,0.00);
        cart.addProduct(product,-1);
        Assert.assertEquals(cart.getTotalValue(),108.92,0.00);
        cart.addProduct(product,-100);
        //quantity of product can not be less than 0
        Assert.assertEquals(cart.getTotalValue(),8.97,0.00);
    }

    @Test
    public void cartSchouldReturnTotalQuantityOfProductsStoredInCart(){
        cart.addProduct(product,4);
        cart.addProduct(new Product("Cola","Coca cola 0,5l",2.99),3);
        cart.addProduct(product,2);
        Assert.assertEquals(cart.getTotalQuantity(),9);
        cart.addProduct(product,-1);
        Assert.assertEquals(cart.getTotalQuantity(),8);
        cart.addProduct(product,-20);
        //quantity of product can not be less than 0
        Assert.assertEquals(cart.getTotalQuantity(),3);
    }

    @Test
    public void cartSchouldFindByNameProductAddedToCart(){
        cart.addProduct(product,1);

        Assert.assertEquals(cart.findProductByName("Book").get(),product);
        //if product doesn't exists in cart, method returns empty Optional
        Assert.assertEquals(cart.findProductByName("Product that doesn't exists").isPresent(),false);
    }
}
