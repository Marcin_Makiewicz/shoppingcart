package makiewicz.task.model;


import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ProductParameterizedTest {

    private static Product product;

    @Parameterized.Parameter(value = 0)
    public Product anotherProduct;

    @Parameterized.Parameter(value = 1)
    public boolean result;

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider(){
        return Arrays.asList(new Object[][]{
                {new Product("Book","Good book for kids",19.99),true},
                {new Product(),false},
                {new Product("Book2","Good book for kids",19.99),false},
                {new Product("Book","Good book",19.99),false},
                {new Product("Book","Good book for kids",5.00),false}
        });
    }

    @BeforeClass
    public static void setup(){
        product = new Product("Book","Good book for kids",19.99);
    }

    @Test
    public void equalsSchouldReturn(){
        Assert.assertEquals(product.equals(anotherProduct), result);
    }

    //contract beetween equals() and hashCode() methods
    @Test
    public void whenEqualsReturnsTrueThenHashCodesAreTheSame(){
        if(product.equals(anotherProduct)){
            Assert.assertEquals(product.hashCode(), anotherProduct.hashCode());
        }
    }

    //contract beetween equals() and hashCode() methods
    @Test
    public void whenHashCodesAreNotTheSameThenEqualsReturnFalse(){
        if(product.hashCode() != anotherProduct.hashCode()){
            Assert.assertEquals(product.equals(anotherProduct),false);
        }
    }
}
