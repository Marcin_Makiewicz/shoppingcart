package makiewicz.task.model;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CartPositionTest {

    private CartPosition cartPosition;
    private Product product;

    @Before
    public void setup(){
        cartPosition = new CartPosition();
        product = new Product("Book","Good book for kids",19.99);
    }

    @Test
    public void CheckEmptyCartPosition(){
        Assert.assertEquals(cartPosition.getProduct(),null);
        Assert.assertEquals(cartPosition.getQuantity(),0);
    }

    @Test
    public void CartPositionSchouldReturnInformationsAboutSetProduct(){
        cartPosition.setProduct(product);
        Assert.assertEquals(cartPosition.getProduct(),product);
        Assert.assertEquals(cartPosition.getQuantity(),0);
        Assert.assertEquals(cartPosition.getAmount().doubleValue(),0.00,0);
    }

    @Test
    public void CartPositionSchouldReturnInformationsAboutSetProductWithQuantity(){
        cartPosition.setProduct(product);
        cartPosition.setQuantity(1);
        Assert.assertEquals(cartPosition.getProduct(),product);
        Assert.assertEquals(cartPosition.getQuantity(),1);
        Assert.assertEquals(cartPosition.getAmount().doubleValue(),19.99,0);
    }

    @Test
    public void CartPositionSchouldReturnInformationsAboutSetProductWithQuantity5(){
        cartPosition.setProduct(product);
        cartPosition.setQuantity(5);
        Assert.assertEquals(cartPosition.getProduct(),product);
        Assert.assertEquals(cartPosition.getQuantity(),5);
        Assert.assertEquals(cartPosition.getAmount().doubleValue(),99.95,00.00);
    }

}
