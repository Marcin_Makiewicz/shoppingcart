package makiewicz.task.model;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class ProductTest {

    private Product product;

    @Before
    public void setup(){
        product = new Product("Book","Good book for kids",19.99);
    }

    @Test
    public void getNameSchouldReturnBook(){
        Assert.assertEquals(product.getName(),"Book");
    }

    @Test
    public void getDescriptionSchouldReturn(){
        Assert.assertEquals(product.getDescription(),"Good book for kids");
    }

    @Test
    public void getPriceSchouldReturn(){
        Assert.assertEquals(product.getPrice(),19.99,0);
    }

    @Test
    public void setNameSchouldChangeNameValue(){
        product.setName("AnotherBook");
        Assert.assertEquals(product.getName(),"AnotherBook");
    }

    @Test
    public void setDescriptionSchouldChangeDescriptionValue(){
        product.setDescription("Description changed");
        Assert.assertEquals(product.getDescription(),"Description changed");
    }

    @Test
    public void setPriceSchouldChangePriceValue(){
        product.setPrice(5.00);
        Assert.assertEquals(product.getPrice(),5.00,0);
    }
}
